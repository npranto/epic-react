# What Did I Learn?

## react-fundamentals

**DOM - Document Object Model**

DOM or Document Object Model is key to allowing dynamic capability in modern web development space. It's an API or Application Programming Interface that allows developers to use JavaScript functions to manipulate or change content for a web page

Example:
```js
document.querySelector('body')
  .addEventListener('click', function() {
    // do something... 
  })
```
The above code adds a event listener to the <body> tag in the HTML. So, if anyone click on the <body> element, it will trigger the anonymous function right away. In here, `document.querySelector()` and `<ELEMENT>.addEventListener` are DOM API functionalities that allows developers to execute an action when an event occurs

**How Browsers Parse HTML Content**

- browser usually parses and runs each line one by one inside an HTML document
- when it comes across a JavaScript file, it blocks the HTML parser, downloads the JavaScript file, executes it and then unblocks the HTML parser to continue parsing the rest of the document
- thus, we put JavaScript script tags at the end of the <body> tag, so all HTML is rendered first before executing JavaScript

**Normal vs. Async vs. Defer Script Loading**

![Normal vs. Async vs. Defer Script Loading](https://i.postimg.cc/vZNQTPnL/Screen-Shot-2021-11-02-at-1-03-52-PM.png "Normal vs. Async vs. Defer Script Loading")
- By default, or on "normal" mode, JavaScript is downloaded and executed as soon as the browser hits a script tag
- On "async" mode, JavaScript is downloaded in parallel to HTML parsing, but it's execution is not guaranteed at a certain time (can either be executed before the HTML parsing is complete or after or even after 'domcontentloaded' event is fired)
  - use `async` when the order is which a script is run does not matter, you just want to load it at some point
- On "defer" mode, JavaScript is downloaded in parallel to HTML parsing, but is executed AFTER HTML parsing is complete and BEFORE 'domcontentloaded' event is fired  
  - use `defer` for most reliable execution of a script

We can set `setAttribute` function to add attributes to an element with JavaScript

While it's great to be able to dynamically generate HTML with DOM API, it's kind of painful to create elements and compose them together because it requires so much code, not to mention messy code.

**JSX**

JSX or JavaScript Extension really makes it easier for developers to visualize and write components similar to HTML. This is a lot easier syntax than the default `React.createElement` way, but behind the scenes, Babel transpiler is used to convert JSX => `React.createElement`. 

Using attribute `type="text/babel"` in the script tag tells Babel to transpile and convert JSX to JavaScript

Quickly spread a bunch of props to a JSX element with `{...props}`:

Example:
```jsx
const props = { 
  className: 'container', 
  id: "element",
  children: 'Hello',
};
const element = <div {...props} />
```

Use `propTypes` to give more meaningful instruction for a custom component. It gives a documentation like guidance to users who are using a component. In addition, it also attempts to prevent users from incorrectly using a component by either setting incorrect props or values for those props.

Note: `propTypes` comes in with as part of the React package in development mode only. It is not added to the production React script however because of performance reasons.

`prop-types` is a separate package maintained by the React team that contains a shared list of validations usually needed for different types of props (i.e, strong, number, function and so on) - Ref: https://npm.im/prop-types

React does NOT allow returning multiple elements next to each other when we return JSX from a function or component.

Example:
*BAD*
```js
return (
  <span>Hello</span>
  <span>World</span>
)
```
*GOOD*
`React.Fragments` allows us to return multiple elements side by side
```js
  <React.Fragment> 
    <span>Hello</span>
    <span>World</span>
  <React.Fragment>
```

**Forms**

`event.preventDefault()` prevents default behavior of form submit, which is to make a GET request right away with input `name` and `values` added as query string parameters in the URL. Also, this behavior does cause the browser to reload the page as well.

React form submit event returns a `SyntheticEvent`, which is not the same as the default browser form submit event. To access the browser submit event, use `e.nativeEvent` inside form submit handler.

Adding `id` or `name` attributes to forms stores the input values under form element's `elements` property

Example:
```jsx
  handleSubmit(e) {
    e.preventDefault();
    console.log( e.target.elements.email.value ); // logs value of email input
    console.log( e.target.elements.password.value ); // logs value of password input
  }

  <form onSubmit={handleSubmit}>
    ...
    <input type="email" name="email" />
    <input type="password" id="password" />
    ...
  </form>
```

If you do NOT want an input value to be editable (readonly), then you can either add `readyOnly` attribute to that input OR, add a `defaultValue` attribute to that input. Either is fine, but `defaultValue` is the default standard. But, do not use `value` attribute. That attribute usually goes well with `onChange` handler.

`key` prop is required for each element in an array when rendering JSX. It helps React keep track of elements you filter or move around through the the app cycle. Key (no pun intended!) takeaways - 1. do not use `index` of an array as the value for `key` prop, 2. using randomly generated id would potential mismatch of index elements, but it's still not the proper fix. 3. Try to use specific id based on the elements themselves, so no matter how to alter that array, the key for each item would be unique based on the item itself. 

**Hooks**

Hooks help in making React much more reusable tool to build user interfaces. It intended to solve 3 main issues 
  - sharing same functionality or logic between multiple components
  - simplifying lifecycle hooks
  - confusion of `this` keyword and its usage within a class environment

Popular Hooks:
- `useState`
  - to keep track of local state in a functional component
- `useEffect`
  - to handle side effects, talking to HTTP, or local storage or browser related APIs and so on
- `useRef`
  - to get direct reference to DOM nodes and interact with them over time
- `useContext`
- `useReducer`

## react-hooks

**useState**

```jsx
  const [ name, setName ] = React.useState('')
  // initial value of `name` is what you pass into React.useState() during initialization
  // calling setName() updates the `name` state and re-renders the component that useState is initialized in
```

lazy state initialization helps to optimize a component level code by only running once as the component mounts and not on followup re-renders.

```jsx
  const [ name, setName ] = React.useState(() => {
    // compute expensive calculation
  })
```

Key Takeaways:
- hooks in general, including React hooks can NOT be used inside a class component, rather only function components can leverage it
- do not use `useState` hooks inside a conditional (if/else) or loops (for/while)
- `useState` can take in either a value or a function (AKA lazy initializer) that returns the initial value of state
- `useState` hook's updater function can take either take in an updated state value or a callback function that can leverage a few parameters to compute the updated state
```jsx
const [name, setName] = React.useState('');
// ...
setName('John');
// OR
setName((prevName) => {
  // compute next state with `prevName`, if needed
});
```
- `useState` does NOT automatically merge update objects

```jsx
// do NOT do this... 
setState(() => ({ name: 'Bryan' }))
// Instead, DO this...
setState(() => ({ ...state, name: 'Bryan' }))
```

**useEffect**

Run any side effects for your component inside useEffects. Side effects as in fetching data, event listeners, checking for a specific `prop` change and so on

dependency array helps to make useEffect hook more efficient by only executing the code in it if there is any chance to props provided inside the array

```jsx
  React.useEffect(() => {
    // this only gets executed if 
    // 1. when function gets executed initially OR 
    // 2. when the `name` prop changes
  }, [name])
```

In general, use `JSON.stringify` and `JSON.parse` to store and retrieve key/value pairs in sessionStorage or localStorage. This way, the data type is stored during parse and stringified during storage.

Key Takeaways:
- use an empty `[]` as a second argument to `useEffect` hook to perform action only the first time a component mounts, basically same as `componentDidMount` or `componentWillMount`
- use dependency array to optimize render cycle of a component, so use efficiently to only update a component based on certain `prop` or `state`
- remember, React uses shallow comparison to check if dependency array changed or not, so using an object as a property inside the dependency array might not be ideal to save re-render cycle
- return a cleanup function to emulate actions to do on component unmount
```jsx
useEffect(() => {
  // run effect actions here...
  return () => {
    // run actions on component unmount
  }
})
```
- use `useEffect` to set event listeners, browser services (localStorage, setTimeout), HTTP requests or to simply interact with DOM elements
```jsx
useEffect(() => {
  window.addEventListener('click', () => {...})
  return () => {
    window.removeEventListener('click', () => {...})
  }
})
```

**React Hooks Flow**

![React Hooks Flow](https://raw.githubusercontent.com/donavon/hook-flow/master/hook-flow.png)

**Lifting State**

When 2 sibling components needs to share a piece of state, we need to initialize that state on the parent scope of the 2 components, so it can be passed down to both children easily.

Example:
```jsx
const Parent = () => {
  const [ name, setName ] = useState('John');
  return (
    <>
      // `setName` is passed to child components in case they need to request 
      // to change the `name` prop on the parent scope. Generally, child 
      // components are NOT allowed to pass data up the tree to its parent 
      // b/c React emphasizes one-way data flow only, but functional callbacks 
      // are the only way to achieve that if a child ever needs to talk to its /
      // parent.
      <Child1 name={name} setName={setName} />
      <Child2 name={name} setName={setName} />
    </>
  )
}
```

Try to keep your state as close to where it is being used if you can. Keeping states close will potentially bring some performance gains by eliminating unnecessary re-renders for 1 or more components.

**useRef**
- hook to keep track of a variable and its changes over time without any re-renders
- useful to track actual DOM nodes in a React component in case we need to have direct interaction with it
- generally, usage of useRef to interact with DOM nodes should occur inside `useEffect` callback

Gotcha of `useEffect`

Do not return a promise from the callback function...

```jsx
// this does not work, don't do this:
React.useEffect(async () => {
  const result = await doSomeAsyncThing()
  // do something with the result
})
```
Instead...

```jsx
React.useEffect(() => {
  async function effect() {
    const result = await doSomeAsyncThing()
    // do something with the result
  }
  effect()
})
```

**Error Boundary**

```jsx
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hasError: false,
      error: null,
    }
  }

  static getDerivedStateFromError(error) {
    return {hasError: true, error}
  }

  render() {
    if (this.state.hasError) {
      return (
        <div role="alert">
          There was an error:{' '}
          <pre style={{whiteSpace: 'normal'}}>
            {this.state?.error?.message || 'lol'}
          </pre>
        </div>
      )
    }

    return this.props.children
  }
}
```

## Question(s)

- When we make a call to useState updater function, how can we access the updated state right after that?
- When we use a lazy initializer to set a state inside useState hook, can we do some heavy computation (i.e., long executing for loop, setTimeout call or HTTP calls)? What happens in each scenario? What to avoid using? What are best practices?
- What does the `useRef` hook really do? What are some use cases for it? Any way I can misuse it that I should be aware of?
  - In React, most common use case for `ref` prop or use of `useRef` hook is to reach into the actual DOM node.
    
    Example:
  
    ```jsx
      function MyDiv() {
        const myDivRef = React.useRef()
        React.useEffect(() => {
          const myDiv = myDivRef.current
          // myDiv is the div DOM node!
          console.log(myDiv)
        }, [])
        return <div ref={myDivRef}>hi</div>
      }
    ```
  - use `useRef` whenever you need to keep track of a DOM node and change the DOM node without forcing a re-render of a component
- When does the cleanup callback functions within `useEffect` get executed? Is it when a component is unmounted only, what about when a state change?
- How to properly add and remove event listeners in React component useEffect hook?
- Why does useEffect hook enforce us to not use async callbacks? Also, why does using async inside the function works, but not when the callback itself is an async?
- 














